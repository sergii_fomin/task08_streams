package com.fomin.task1;

import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        FunctionalInterface functionalInterface = (a, b, c) -> Stream.of(a, b, c).max(Integer::compareTo).get();
        System.out.println(functionalInterface.singleMethod(1, 2, 3));
        functionalInterface = (a, b, c) -> (int) Stream.of(a, b, c).mapToInt(Integer::intValue).average().getAsDouble();
        System.out.println(functionalInterface.singleMethod(1, 2, 3));
    }
}
