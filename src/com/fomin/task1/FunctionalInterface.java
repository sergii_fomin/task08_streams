package com.fomin.task1;

interface FunctionalInterface {
    int singleMethod(int a, int b, int c);
}
