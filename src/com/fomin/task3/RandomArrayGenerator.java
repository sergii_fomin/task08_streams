package com.fomin.task3;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.Random;

public class RandomArrayGenerator {

    protected static int[] generateRandomIntArray(int arrayLength, int upperBound) {
        int[] array = new int[arrayLength];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(upperBound);
        }
        return array;
    }

    private static IntSummaryStatistics getStatistic(int[] array) {
        return Arrays.stream(array).summaryStatistics();
    }

    public static double average(int[] array) {
        return getStatistic(array).getAverage();
    }

    public static int max(int[] array) {
        return getStatistic(array).getMax();
    }

    public static int min(int[] array) {
        return getStatistic(array).getMin();
    }

    public static long sum(int[] array) {
        return getStatistic(array).getSum();
    }

    public static long countValuesBiggerThanAverage(int[] array, double average) {
        return Arrays.stream(array).filter(i -> i > average).count();
    }


}
