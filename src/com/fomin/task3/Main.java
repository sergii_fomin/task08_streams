package com.fomin.task3;

import java.util.Arrays;

import static com.fomin.task3.RandomArrayGenerator.*;

public class Main {

    public static void main(String[] args) {

        int[] array = generateRandomIntArray(20, 100);
        System.out.println(Arrays.toString(array));

        System.out.println("Max value: " + max(array));
        System.out.println("Min value: " + min(array));
        System.out.println("Average value: " + average(array));
        System.out.println("Sum: " + sum(array));
        System.out.println("Count of values bigger than average value: "
                + countValuesBiggerThanAverage(array, average(array)));

    }
}
