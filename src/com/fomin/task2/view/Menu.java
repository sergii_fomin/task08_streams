package com.fomin.task2.view;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

public class Menu {
    private String name;
    private String text;
    private LinkedHashMap<String, Runnable> actionsMap = new LinkedHashMap<>();

    private Menu(final String name, final String text) {
        this.name = name;
        this.text = text;
    }

    public Menu() {

        Menu menu = new Menu("Task 2", "menu");

        menu.putAction("Command 1", this::command1);
        menu.putAction("Command 2", this::command2);
        menu.putAction("Command 3", this::command3);
        menu.putAction("Command 4", this::command4);

        activateMenu(menu);
    }

    private void command1() {
    }

    private void command2() {
    }

    private void command3() {
    }

    private void command4() {
    }

    private void activateMenu(final Menu menu) {
        System.out.println(menu.generateMenu());
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int actionNumber = scanner.nextInt();
            menu.executeAction(actionNumber);
        }
    }

    private void putAction(final String name, final Runnable action) {
        actionsMap.put(name, action);
    }

    private String generateMenu() {
        StringBuilder builder = new StringBuilder();
        builder.append(name).append(" ");
        builder.append(text).append(":\n");
        List<String> actionNames = new ArrayList<>(actionsMap.keySet());
        for (int i = 0; i < actionNames.size(); i++) {
            builder.append(String.format(" %d: %s%n", i + 1, actionNames.get(i)));
        }
        return builder.toString();
    }

    private void executeAction(int actionNumber) {
        actionNumber -= 1;
        if (actionNumber < 0 || actionNumber >= actionsMap.size()) {
            System.out.println("Wrong menu option: " + actionNumber);
        } else {
            List<Runnable> actions = new ArrayList<>(actionsMap.values());
            actions.get(actionNumber).run();
        }
    }

}
